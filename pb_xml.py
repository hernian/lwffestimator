
import subprocess

mo_tf_path = '/opt/intel/openvino/deployment_tools/model_optimizer/mo_tf.py'
pb_file = './model/lwff_model.pb'
output_dir = './model'
input_shape = '[1,48,48,1]'
cmd = f'python {mo_tf_path} --input_model {pb_file} --output_dir {output_dir} ' \
      f'--input_shape {input_shape} --data_type FP16'
subprocess.check_call(cmd)
