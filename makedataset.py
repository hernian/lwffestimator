""" モノクロ画像(3chのpng)を複数読み込んで、tensorflowで使える1chのモノクロ画像の集合（配列）に変換する

　データ(csv)の形式
    filename: 画像のファイル名
    class_number: クラス番号 (0:通常歩き, 1:スマホながら歩き)
"""

from typing import Generator, Tuple
import sys
import cv2
import numpy as np


def cvs_reader(filename_in: str) -> Generator[Tuple[str, int], None, None]:
    with open(filename_in) as fs:
        for line in fs:
            tokens = line.split(',')
            yield (tokens[0], int(tokens[1]))


def make_dataset(filename_in: str, filename_out: str, size_img: Tuple[int, int]) -> np.ndarray:
    w_img = size_img[0]
    h_img = size_img[1]
    dataset = np.zeros((0, h_img, w_img, 1), dtype=np.uint8)
    classset = np.zeros((0), dtype=np.uint8)
    reader = cvs_reader(filename_in)
    for row in reader:
        filename_img = row[0]
        index_class = row[1]
        img_3ch = cv2.imread(filename_img)
        img_1ch = cv2.cvtColor(img_3ch, cv2.COLOR_RGB2GRAY)
        data = img_1ch.reshape((1, h_img, w_img, 1))
        dataset = np.vstack((dataset, data))
        classset = np.append(classset, index_class)
    np.savez(filename_out, x=dataset, y=classset)


if __name__ == "__main__":
    filename_in = sys.argv[1]
    filename_out = sys.argv[2]
    make_dataset(filename_in, filename_out, (48, 48))
